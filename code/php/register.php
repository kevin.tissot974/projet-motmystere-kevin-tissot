<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html lang="en">
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Website CSS style -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- Website Font style -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		<link rel="stylesheet" href="../css/register.css">
		<!-- Google Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

		<title>Register</title>
	</head>
	<body>
		<?php
			// 1 - Créer les variables de connexion
			$serverHote = 'localhost';
			$serverUser = 'root';
			$serverPassword = '';
			$serverName = 'mystery_word';

			// 2 - Créer la variable de connexion
			$bdd = mysqli_connect('localhost','root','','mystery_word'); //Versions sans variables de connexion
			// $bdd = mysqli_connect($serverHote,$serverUser,$serverPassword,$serverName); //Versions avec variables de connexion

			// 3 - Tester la connexion
			if (mysqli_connect_errno()) {
				echo 'connexion échoué'.mysqli_connect_errno();
			}else {
				echo 'Connexion réussi';
			}

			// 4 - Récupérer les information de l'utilisateur du server
			if (!empty($_POST["first_name"]) && !empty($_POST["last_name"]) && !empty($_POST["email"]) && !empty($_POST["password"])) {
				
				// 4-1 Stockage des données utilisateur dsans la méthode post
				$first_name = $_POST["first_name"];
				$last_name = $_POST["last_name"];
				$email = $_POST["email"];
				$password = $_POST["password"];
				
				// 4-2 Hashage du password
				$passwordHash = password_hash($password,PASSWORD_DEFAULT);

				/*PASSWORD_DEFAULT - Utilisez l'algorithme bcrypt (par défaut à partir de PHP 5.5.0). Notez que cette constante
				 est conçue pour changer au fil du temps à mesure que de nouveaux algorithmes plus puissants sont ajoutés à PHP.
				 Pour cette raison, la longueur du résultat résultant de l’utilisation de cet identifiant peut changer au fil du temps.
				 Par conséquent, il est recommandé de stocker le résultat dans une colonne de base de données pouvant contenir plus de 60
				 caractères (255 caractères seraient un bon choix).*/

				 /*htmlentities() est identique à la fonction htmlspecialchars(), sauf que tous les caractères qui ont des équivalents
				 en entités HTML sont effectivement traduits. La fonction get_html_translation_table() peut être utilisée pour retourner
				 la table de traduction utilisée en fonction des constantes flags fournies.*/

				 /*addslasches = Son but principal est de reconnaître dans une chaîne de caractères les guillemets(‘), les guillemets doubles(“)
				 ainsi que les backslash(\) car l'utilité de ces caractères spéciaux sont d’ouvrir et fermer une chaîne de caractères.
				 Du coup, la fonction reconnaît que si il est utile qu’un tel est une simple apostrophe d’un mot ou non.
				 Celà évite les erreurs ou une altération indésirable du code. */

				// 5 - Création de la requête SQL avec les protection contre les injections
				$request = "INSERT INTO User(first_name, last_name, email, password) VALUES
				('". htmlentities(addslashes($first_name), ENT_QUOTES)."',
				'". htmlentities(addslashes($last_name), ENT_QUOTES)."',
				'". htmlentities(addslashes($email), ENT_QUOTES)."',
				'". htmlentities(addslashes($passwordHash), ENT_QUOTES)."')";


				// 6 - On vérifie que que la connexion et ok est que la requête est OK alors on redirige l'utilisateur vers login.php 
				// 6.1 - Vérification
				$result = mysqli_query($bdd, $request);

				//6-2 Test pour redirection
				if ($result) {
					header("location: ./login.php");
				}else {
					$error = 'erreur merci de compléter';
				}

			}
		?>
		<section class="container">
			<article class="row main">
				<div class="main-login main-center">
				<h5>Sign up once and watch any of our free demos.</h5>
					<form class="" method="post" action="#">
						
						<div class="form-group">
							<label for="first_name" class="cols-sm-2 control-label">Your First Name</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="first_name" id="first_name"  placeholder="Enter your First Name"/>
								</div>
							</div>
						</div>

                        <div class="form-group">
							<label for="last_name" class="cols-sm-2 control-label">Your Last Name</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="last_name" id="last_name"  placeholder="Enter your Last Name"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Your Email</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input type="email" class="form-control" name="email" id="email"  placeholder="Enter your Email"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password"/>
								</div>
							</div>
						</div>
                        <div class="form-group">
							<a href="./login.php" class="text-info">Login here</a><br>
                            <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">
                        </div>
						<?php
							echo $error;
						?>
					</form>
				</div>
			</article>
		</section>

		 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	</body>
</html>