<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <?php

        // 1 - Créer la variable de connexion
        $bdd = mysqli_connect('localhost','root','','mystery_word'); //Versions sans variables de connexion
        // $bdd = mysqli_connect($serverHote,$serverUser,$serverPassword,$serverName); //Versions avec variables de connexion

        // 2 - Tester la connexion
        if (mysqli_connect_errno()) {
            echo 'connexion échoué'.mysqli_connect_errno();
        }
        
        // Vous devez faire un select pour afficher le mot mystere affiché en bdd ! =)
        // 3 - Créer la requete SQL pour récupérer le mot mystère dans la base de données
        $query_mystere = "SELECT word FROM mystery_word ";

        $result = mysqli_query($bdd,$query_mystere);

        //4 - On récupère l'information via une boucle et while et une fonction fetch

        while ($result = mysqli_fetch_array($query_mystere)) {
        /*
        if ($result = mysqli_fetch_assoc($result)) {

            $result['word'] = $result ;
        }*/
    ?>

    <p> Je veux connaitre le mot mystere, donc affichez le ici : <?php echo $result[0]; }?></p>

    <br>

    <a href="./update_profile.php" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">Modification du profile</a>
    
</body>
</html>